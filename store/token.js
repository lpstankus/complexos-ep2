export const state = () => ({ jwt: '' })

export const mutations = {
  jwt(state, value) { state.jwt = value }
}
