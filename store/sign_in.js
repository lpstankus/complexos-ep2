export const state = () => ({
  email: '',
  password: '',
})

export const mutations = {
  clear(state) {
    state.email = '';
    state.password = '';
  },
  email(state, value) { state.email = value },
  password(state, value) { state.password = value },
}

export const actions = {
  async send(context) {
    const response = await this.$axios.$post(
      '/login',
      {
        email: context.state.email,
        password: context.state.password,
      }
    );
    const id = JSON.parse(atob(response.accessToken.split('.')[1])).sub
    this.commit("token/jwt", response.accessToken, { root: true });
    this.$router.push('/users/' + id);
  }
}
